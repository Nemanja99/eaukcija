package model;

import java.util.ArrayList;
import java.util.List;

public class Aukcija {
	
	private Long id;
	private String naziv;
	private String autor;
	private int trenutna_cena;
	private String trajeDo;
	
	private List<TipDela> tipoviDela = new ArrayList<>();
	
	public Aukcija(String naziv, String autor, int trenutna_cena, String trajeDo) {
		this.naziv = naziv;
		this.autor = autor;
		this.trenutna_cena = trenutna_cena;
		this.trajeDo = trajeDo;
	}
	
	public Aukcija(Long id, String naziv, String autor, int trenutna_cena, String trajeDo) {
		this.id = id;
		this.naziv = naziv;
		this.autor = autor;
		this.trenutna_cena = trenutna_cena;
		this.trajeDo = trajeDo;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Aukcija other = (Aukcija) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public String getAutor() {
		return autor;
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	
	public int getTrenutna_cena() {
		return trenutna_cena;
	}
	public void setTrenutna_cena(int trenutna_cena) {
		this.trenutna_cena = trenutna_cena;
	}
	public String getTrajeDo() {
		return trajeDo;
	}
	public void setTrajeDo(String trajeDo) {
		this.trajeDo = trajeDo;
	}
	
	public List<TipDela> getTipoviDela() {
		return tipoviDela;
	}

	public void setTipoviDela(List<TipDela> tipoviDela) {
		this.tipoviDela.clear();
		this.tipoviDela.addAll(tipoviDela);
	}
	
	@Override
	public String toString() {
		return "Aukcija [id=" + id + ", naziv=" + naziv + ", autor=" + autor + " , trenutnCena=" + trenutna_cena + ", trajeDo=" + trajeDo +"]";
	}

}
