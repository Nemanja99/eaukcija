package service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

/*import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
*/

import model.Aukcija;
import model.TipDela;
import service.AukcijaInterfejs;
import service.TipDelaInterfejs;

/*@Service
@Qualifier("memorijaAukcija")*/
public class InMemoryAukcijaService implements AukcijaInterfejs{

	private Map<Long, Aukcija> aukcije = new HashMap<>();
	private long nextId = 1L;
	
	//@Autowired
	private TipDelaInterfejs tipService;
	
	@PostConstruct
    private void iniDataForTesting() {
		Aukcija aukcija;

		aukcija = new Aukcija("Lokvanji","Klod Mone", 200000, "2020/12/02 12:00");
		aukcija.getTipoviDela().add(tipService.findOne(1L));
		aukcija.getTipoviDela().add(tipService.findOne(2L));
		aukcija.getTipoviDela().add(tipService.findOne(5L));
		save(aukcija);

		aukcija = new Aukcija("Suncokreti","Van Gog", 1000000, "2020/12/02 18:00");
		aukcija.getTipoviDela().add(tipService.findOne(1L));
		aukcija.getTipoviDela().add(tipService.findOne(4L));
		save(aukcija);

		aukcija = new Aukcija("David","Mikelandjelo", 1000000, "2020/12/02 20:00");
		aukcija.getTipoviDela().add(tipService.findOne(4L));
		save(aukcija);
    }
	
	@Override
	public Aukcija findOne(Long id) {
		return aukcije.get(id);
	}

	@Override
	public List<Aukcija> findAll() {
		return new ArrayList<Aukcija>(aukcije.values());
	}

	@Override
	public Aukcija save(Aukcija aukcija) {
		
		if (aukcija.getId() == null) {
			aukcija.setId(nextId++);
		}
		aukcije.put(aukcija.getId(), aukcija);
		return aukcija;
	}

	@Override
	public List<Aukcija> save(List<Aukcija> aukcije) {
		List<Aukcija> rezultat = new ArrayList<>();
		for (Aukcija davidAukcija: aukcije) {
			
			Aukcija saved = save(davidAukcija);

			if (saved != null) {
				rezultat.add(saved);
			}
		}

		return rezultat;
	}
	
	@Override
	public Aukcija update(Aukcija aukcija) {
		aukcije.put(aukcija.getId(), aukcija);
		return aukcija;
	}

	@Override
	public List<Aukcija> update(List<Aukcija> aukcije) {
		List<Aukcija> rezultat = new ArrayList<>();
		for (Aukcija davidAukcija: aukcije) {
			
			Aukcija updated = update(davidAukcija);

			if (updated != null) {
				rezultat.add(updated);
			}
		}

		return rezultat;
	}

	@Override
	public Aukcija delete(Long id) {
		if (!aukcije.containsKey(id)) {
			throw new IllegalArgumentException("invalid id");
		}

		Aukcija aukcija = aukcije.get(id);
		if (aukcija != null) {
			aukcije.remove(id);
		}
		
		return aukcija;
	}

	@Override
	public List<Aukcija> deleteAll(TipDela tipDela) {
		List<Aukcija> aukcijeZaUklanjanje = new ArrayList<>();
		for (Aukcija davidAukcija: aukcije.values()) {
			if (davidAukcija.getTipoviDela().contains(tipDela)) {
				aukcijeZaUklanjanje.add(davidAukcija);
			}
		}		
		List<Aukcija> rezultat = new ArrayList<>();
		for (Aukcija davidAukcija: aukcijeZaUklanjanje) {
			Aukcija uklonjenaAukcija = aukcije.remove(davidAukcija.getId());
			rezultat.add(uklonjenaAukcija);
		}

		return rezultat;
	}

	@Override
	public void delete(List<Long> ids) {
		for (Long itID: ids) {

			delete(itID);
		}
	}

	
	public List<Aukcija> find(String naziv, Long tipId, Integer trenutna) {
		if (naziv == null) {
			naziv = "";
		}
		if (tipId == null) {
			tipId = 0L;
		}
		if (trenutna == null) {
			trenutna = 0;
		}
		
		List<Aukcija> rezultat = new ArrayList<>();
		for (Aukcija itAukcija: aukcije.values()) {
			if (!itAukcija.getNaziv().toLowerCase().contains(naziv.toLowerCase())) {
				continue;
			}
			if (tipId > 0) {
				boolean pronadjen = false;
				for (TipDela itDela: itAukcija.getTipoviDela()) {
					if (itDela.getId() == tipId) {
						pronadjen = true;
						break;
					}
				}
				if (!pronadjen) {
					continue;
				}
			}
			if (!(itAukcija.getTrenutna_cena() >= trenutna)) {
				continue;
			}

			rezultat.add(itAukcija);
		}

		return rezultat;
	}

	@Override
	public List<Aukcija> findByTipDelaId(Long tipDelaId) {
		if (tipDelaId == null) {
			tipDelaId = 0L;
		}
		
		List<Aukcija> rezultat = new ArrayList<>();
		for (Aukcija davidAukcija: aukcije.values()) {
			if (tipDelaId > 0) {
				boolean pronadjen = false;
				for (TipDela itDela: davidAukcija.getTipoviDela()) {
					if (itDela.getId() == tipDelaId) {
						pronadjen = true;
						break;
					}
				}
				if (!pronadjen) {
					continue;
				}
			}

			rezultat.add(davidAukcija);
		}

		return rezultat;
	}

	@Override
	public List<Aukcija> find(String naziv, Long tipDelaId) {
		// TODO Auto-generated method stub
		return null;
	}

}