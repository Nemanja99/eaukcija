package service;

import java.util.List;

import model.Aukcija;
import model.TipDela;

public interface AukcijaInterfejs {

	Aukcija findOne(Long id);
	List<Aukcija> findAll();
	Aukcija save(Aukcija aukcija);
	List<Aukcija> save(List<Aukcija> aukcije);
	Aukcija update(Aukcija aukcija);
	List<Aukcija> update(List<Aukcija> aukcije);
	Aukcija delete(Long id);
	List<Aukcija> deleteAll(TipDela tipDela);
	void delete(List<Long> ids);
	List<Aukcija> find(String naziv, Long tipDelaId);
	List<Aukcija> findByTipDelaId(Long zanrId);
	
}
