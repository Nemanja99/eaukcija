package service;

import java.util.List;

import model.TipDela;

public interface TipDelaInterfejs {

	TipDela findOne(Long id);
	List<TipDela> findAll();
	List<TipDela> find(Long[] ids);
	TipDela save(TipDela tipDela);
	List<TipDela> save(List<TipDela> tipoviDela);
	TipDela update(TipDela tipDela);
	List<TipDela> update(List<TipDela> tipoviDela);
	TipDela delete(Long id);
	void delete(List<Long> ids);
	List<TipDela> find(String naziv);
	
}
